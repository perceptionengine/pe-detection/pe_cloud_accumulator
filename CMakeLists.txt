cmake_minimum_required(VERSION 3.5)
project(pe_cloud_accumulator)

set(CMAKE_CXX_STANDARD 14)

find_package(catkin REQUIRED COMPONENTS
   pcl_conversions
   pcl_ros
   roscpp
   sensor_msgs
   std_msgs
   tf
   tf2
   tf2_eigen
   tf2_ros
)

catkin_package(CATKIN_DEPENDS
   pcl_conversions
   pcl_ros
   roscpp
   sensor_msgs
   std_msgs
   tf
   tf2
   tf2_eigen
   tf2_ros
)

find_package(Qt5Core REQUIRED)
find_package(OpenCV REQUIRED)
find_package(PCL 1.7 REQUIRED)
find_package(OpenMP)

include_directories(
   ${catkin_INCLUDE_DIRS}
)

add_executable(cloud_accumulator
   cloud_accumulator/src/cloud_accumulator.cpp
)

target_include_directories(cloud_accumulator PRIVATE
   cloud_accumulator/include
   ${PCL_INCLUDE_DIRS}
)

target_link_libraries(cloud_accumulator
   ${catkin_LIBRARIES}
   ${PCL_LIBRARIES}
   ${Qt5Core_LIBRARIES}
)

install(
   TARGETS
   cloud_accumulator
   ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
   LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
   RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(DIRECTORY launch/
    DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/launch
    PATTERN ".svn" EXCLUDE
)