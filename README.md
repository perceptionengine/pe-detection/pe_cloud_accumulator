# Perception Engine's Cloud Accumulator

## Requirements

1. Point cloud
1. TF (pointcloud (1.) -> map/world/slam)

## How to launch

```
roslaunch pe_cloud_accumulator cloud_accumulator.launch
```

## Params

|Parameter| Type| Description|Default|
----------|-----|--------|---|
|`buffer_size`|*Int* |Number of clouds to keep in track.|Default `5`|
|`input_point_topic`|*String*|Name of the PointCloud topic to subscribe.|Default `points_raw`|
|`map_frame`|*String*|Name of the Map/World/SLAM frame to keep track of coordinates between cloud scans.|`map`|