//
// Created by amc on 20/11/20.
//

#ifndef PE_CLOUD_ACCUMULATOR_CLOUD_ACCUMULATOR_H
#define PE_CLOUD_ACCUMULATOR_CLOUD_ACCUMULATOR_H

#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_types.h>

#include <tf2/transform_datatypes.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_eigen/tf2_eigen.h>

#include <boost/circular_buffer.hpp>

typedef pcl::PointXYZI PointType;

class CloudAccumulator
{
private:
  const int MIN_BUFFER_SIZE = 1;
  ros::Subscriber points_sub_;
  ros::Publisher accumulated_points_pub_;

  tf2_ros::Buffer tf_buffer_;
  tf2_ros::TransformListener tf_listener_;

  std::string input_point_topic_;
  std::string map_frame_;
  int buffer_size_;

  boost::circular_buffer<pcl::PointCloud<PointType>::Ptr> previous_cloud_buffer_;
  boost::circular_buffer<tf::StampedTransform> previous_tf_buffer_;

  void CloudCallback(const sensor_msgs::PointCloud2ConstPtr& in_sensor_cloud);
  bool ObtainTransform(const std::string& in_target_frame,
                       const std::string& in_source_frame,
                       tf::StampedTransform & out_transform);

public:
  CloudAccumulator();
  void Run();
};

#endif //PE_CLOUD_ACCUMULATOR_CLOUD_ACCUMULATOR_H
