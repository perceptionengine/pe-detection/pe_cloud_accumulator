//
// Perception Engine. All rights reserved 2020
// Created by amc on 20/11/20.
//

#include "cloud_accumulator/cloud_accumulator.h"

bool CloudAccumulator::ObtainTransform(const std::string& in_target_frame,
                                       const std::string& in_source_frame,
                                       tf::StampedTransform & out_transform)
{
  geometry_msgs::TransformStamped transform_stamped;
  try
  {
    transform_stamped = tf_buffer_.lookupTransform(in_target_frame, in_source_frame,
                                                   ros::Time(0), ros::Duration(1.0));

    tf::transformStampedMsgToTF(transform_stamped, out_transform);
  }
  catch (tf2::TransformException& ex)
  {
    ROS_WARN("%s", ex.what());
    return false;
  }
  //out_transform = tf2::transformToEigen(transform_stamped.transform).matrix().cast<float>();
  return true;
}

void CloudAccumulator::CloudCallback(const sensor_msgs::PointCloud2ConstPtr& in_sensor_cloud)
{
  //Obtain Transform from sensor to world
  tf::StampedTransform current_transform;
  const bool succeeded = ObtainTransform(map_frame_, in_sensor_cloud->header.frame_id, current_transform);
  if (!succeeded)
  {
    ROS_ERROR_STREAM_THROTTLE(1, "Failed tf sensor-map");
    return;
  }

  pcl::PointCloud<PointType>::Ptr pcl_current_sensor_cloud_ptr(new pcl::PointCloud <PointType>);
  pcl::fromROSMsg(*in_sensor_cloud, *pcl_current_sensor_cloud_ptr);

  pcl::PointCloud<PointType>::Ptr pcl_current_map_cloud_ptr(new pcl::PointCloud <PointType>);

  pcl_ros::transformPointCloud(*pcl_current_sensor_cloud_ptr, *pcl_current_map_cloud_ptr, current_transform);

  //if success, save current transform, and current cloud
  previous_cloud_buffer_.push_back(pcl_current_map_cloud_ptr);
  previous_tf_buffer_.push_back(current_transform);

  //if there is at least one more cloud in the buffer, transform it to the current space
  if(!previous_cloud_buffer_.empty()
    && previous_cloud_buffer_.size() > 0)
  {
    pcl::PointCloud<PointType> pcl_output;
    for (size_t i = 0; i < previous_cloud_buffer_.size(); i++)
    {
      pcl::PointCloud<PointType> pcl_current = *previous_cloud_buffer_.at(i);
      pcl::PointCloud<PointType> pcl_transformed;

      //tf::Transform delta_transform;
      //delta_transform = previous_tf_buffer_[i] * current_transform;

      pcl_ros::transformPointCloud(pcl_current, pcl_transformed, current_transform.inverse());
      pcl_output += pcl_transformed;
    }

    sensor_msgs::PointCloud2 acc_cloud;
    pcl::toROSMsg(pcl_output, acc_cloud);
    acc_cloud.header = in_sensor_cloud->header;
    accumulated_points_pub_.publish(acc_cloud);
  }




}

void CloudAccumulator::Run()
{

  ros::NodeHandle global_nh;
  ros::NodeHandle private_nh("~");

  ROS_INFO("Initializing Cloud Accumulator...");
  private_nh.param<std::string>("input_point_topic", input_point_topic_, "points_raw");
  ROS_INFO_STREAM("Input point_topic: "<< input_point_topic_);

  private_nh.param<std::string>("map_frame", map_frame_, "map");
  ROS_INFO_STREAM("Map Frame name: "<< map_frame_);

  private_nh.param("buffer_size", buffer_size_, 5);
  ROS_INFO_STREAM("buffer_size: " << buffer_size_);

  std::string out_acc_topic = "points_accumulated";
  accumulated_points_pub_ = global_nh.advertise<sensor_msgs::PointCloud2>(out_acc_topic, 2);
  ROS_INFO_STREAM("Publishing result in: " << out_acc_topic);

  if(buffer_size_ < MIN_BUFFER_SIZE)
  {
    buffer_size_ = MIN_BUFFER_SIZE;
    ROS_WARN_STREAM("Min cloud buffer size is " << MIN_BUFFER_SIZE << ". Setting it to " << MIN_BUFFER_SIZE);
  }
  previous_cloud_buffer_.set_capacity(buffer_size_);
  previous_tf_buffer_.set_capacity(buffer_size_);

  points_sub_ = global_nh.subscribe<sensor_msgs::PointCloud2>(input_point_topic_, 2,
                                                               &CloudAccumulator::CloudCallback, this);

  ros::spin();
}

CloudAccumulator::CloudAccumulator() : tf_listener_(tf_buffer_)
{
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "pe_cloud_accumulator");

  CloudAccumulator app;

  app.Run();

  return 0;
}